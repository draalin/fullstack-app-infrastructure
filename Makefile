.ONESHELL:
.SHELL := /usr/bin/bash

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

local: ## Run docker-compose build locally
	docker-compose up --build -d
	echo "http://localhost:4000 (WEB)"
	echo "http://localhost:5000 (API)"

init: ## Create Terraform initialization
	cd infrastructure/env
	terraform init
	terraform apply --auto-approve

init-destroy: ## Destroy Terraform initialization
	cd infrastructure/env
	terraform destroy --auto-approve

deploy: ## Deploy production Terraform infrastructure
	cd infrastructure/env/production
	source .env
	terraform init
	terraform workspace new production
	terraform workspace select production
	terraform apply --auto-approve

destroy: ## Destroy production Terraform infrastructure
	cd infrastructure/env/production
	terraform destroy --auto-approve