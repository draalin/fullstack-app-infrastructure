variable "project_name" {
  description = "Project Name"
  type        = string
  default     = null
}

variable "domain_name" {
  description = "Domain Name"
  type        = string
  default     = null
}
variable "certificate" {
  description = "Certificate ARN you wish to use"
  type        = string
  default     = null
}
variable "media" {
  description = "S3 Bucket to serve content"
  type        = string
  default     = null
}

variable "web_acl_cf" {
  description = "WAF ACL ID"
  type        = string
  default     = null
}

variable "compress" {
  description = "Whether you want CloudFront to automatically compress content for web requests that include Accept-Encoding: gzip in the request header."
  type        = bool
  default     = null
}

variable "lambda_arn" {
  description = "Lambda Edge ARN for applying cache headers to s3 content"
  type        = string
  default     = null
}
