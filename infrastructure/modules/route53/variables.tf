variable "project_name" {
}

variable "domain_name" {
}

variable "cloudfront_domain_name" {
}

variable "cloudfront" {
}

variable "aws_alb" {
}

variable "bastion_public_dns" {
}

variable "kibana_public_dns" {
  description = "Kibana DNS"
  type        = string
  default     = null
}