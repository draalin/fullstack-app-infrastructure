variable "project_name" {
}

variable "vpc_id" {
}

variable "private_subnet_1" {
}
variable "private_subnet_2" {
}
variable "public_subnet_1" {
}
variable "public_subnet_2" {
}

variable "certificate" {
}

variable "key_name" {
}

variable "instance_type" {
}

variable "desired_capacity" {
}

variable "desired_count" {
}

variable "service_desired" {
}
variable "min_size" {
}
variable "max_size" {
}

variable "asg_desired" {
}
variable "asg_max" {
}
variable "asg_min" {

}
# variable "database_password" {
# }
# variable "google_analytics_credentials" {
# }
# variable "rails_master_key" {
# }
# variable "static_map_api_key" {
# }
# variable "google_maps_api_key" {
# }
# variable "google_analytics_tracking_id" {
# }
# variable "react_app_google_analytics_tracking_id" {
# }

variable "domain_name" {
}

variable "aws_region" {
}

variable "bastion_security_group" {
}

variable "kibana_public_id" {
}
variable "grafana_public_id" {
}