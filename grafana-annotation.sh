#!/bin/bash
set -e

if [ $# -lt 2 ]; then
    echo "Usage: $0 <text> <tag>"
    exit 1
fi

readonly text="$1"
readonly tag="$2"
readonly time="$(date +%s)000"

cat >./payload.json <<EOF
{
    "text": "$text",
    "tags": ["$tag"],
    "time": $time,
    "timeEnd": $time
}
EOF

curl -X POST "$GRAFANA_URL/api/annotations" \
     -H "Authorization: Bearer $GRAFANA_APIKEY" \
     -H "content-type: application/json" \
     -d @./payload.json
